'use strict';

const AWS = require("aws-sdk");

const db = new AWS.DynamoDB();
const client = new AWS.DynamoDB.DocumentClient();


/**
 * Returns whole contents of the table (nam still specified in code, TODO move variables to process.env) except login info.
 */

module.exports.getAllData = (event, context, callback) => {
  const params = {
    ExpressionAttributeValues: {
      ":r": "page",
      ":i": "info",
    },
    FilterExpression: "item_role = :r or item_role = :i",
    TableName: "psychologbarlinek_pages",
  };
  client.scan(params, (error, result) => {
    if (error) {
      console.log("Failed to load data from DB. Error: ", JSON.stringify(error, null, 2));
      callback(error);
    } else {
      console.log("Data loaded succesfully");
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
        },
        body: JSON.stringify({pages: result.Items}),
      });
    }
  });

};
